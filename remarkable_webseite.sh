#!/bin/bash

set -e
set -u

sudo ip link set usb0 up
sudo ip address add 10.11.99.2 dev usb0
sudo ip route add 10.11.99.1 dev usb0
firefox http://10.11.99.1/
