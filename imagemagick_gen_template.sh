#!/bin/bash

#2022-07-15
#prepare an image for reMarkable2 tablet computer. The image can be used as template or could be postprocessed into a PDF for viewing
#
#rotates in image into portrait mode
#resizes to pixeldimensions of remarkable
#puts a white background behind, if the picture is smaller then required dimensions
#reduces colorspace to 12 gray tones
#applies dithering (pixels spread over an area to simulate more color depth), usefull for photo-realistic pictures

in="$1"
outdir=$(realpath "$in" |rev |cut -d/ -f2-|rev)
outfile="$(basename $in).png"
out="$outdir/$outfile"

convert "$in" \
  -rotate '-90>' \
  -geometry 1404x1872 \
  -extent 1404x1872 \
  -background white \
  -gravity center \
  -colors 12 \
  -colorspace gray \
  -dither Riemersma \
 "$out" 

exit 0

