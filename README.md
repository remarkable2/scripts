# reMarkable scripts
2022-07-15

A collection of Linux shell scripts to operate a reMarkable-2-tablet-computer without using remarkable-cloud.

## done list

 * open remarkable file transfer after plugging in an usb cable
 * generate background template to draw on
 * integrated reStream (share your reMarkable screen over either USB or Wifi
 
## wish list

 * first time setup ssh-key
 * upload template automatically
 * generate photo-pdf-album from pictures
 * transfer files using wifi
 * setup restream (monitor sharing with Linux computer) automatically

## imagemagick_gen_template.sh

takes any file (jpg, pdf, png, svg) and converts it into dimensions, colorspace and file format useable as reMarkable2 template
 
## reStream.sh

awesome repository from <insert here> to share your reMarkable-2-screen with a computer. Use this script to hook reMarkable to a beamer!

## remarkable_webseite.sh

Open reMarkable-file-upload-and-download in brower.

Requires:

 * USB-cable plugged in
 * settings>>storage>>usb_web_interface on
 
